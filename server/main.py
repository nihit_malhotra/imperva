from flask import Flask, jsonify
from pymongo import MongoClient
from datetime import datetime
from flask_cors import CORS
import pytz


app = Flask(__name__)
CORS(app)
db_client = MongoClient(host='localhost', port=27017)
db = db_client.get_database('AppData')


@app.route('/')
def app_server():
    return "success"


@app.route('/userList', methods=['GET'])
def get_user_list():
    data_list = db.get_collection('DvdRentalsCustomers').find(
        {},
        {"First Name": 1, "Last Name": 1}
    )
    list_cur = list(data_list)
    return jsonify(list_cur)


@app.route('/user/<int:user_id>', methods=['GET'])
def get_user_data(user_id):
    user_data = db.get_collection('DvdRentalsCustomers').find(
        {"_id": user_id},
        {"Address": 1, "City": 1, "District": 1, "First Name": 1, "Last Name": 1, "Country": 1, "phone": 1}
    )
    list_cur = list(user_data)
    return jsonify(list_cur)


@app.route('/user/<int:user_id>/rented', methods=['GET'])
def get_users_rent_data(user_id):
    user_rent_data = db.get_collection('DvdRentalsCustomers').find(
        {"_id": user_id},
        {"Rentals.Film Title": 1, "Rentals.rentalId": 1}
    )
    list_cur = list(user_rent_data)
    return jsonify(list_cur)


@app.route('/user/<int:user_id>/rented/<int:rent_id>', methods=['GET'])
def get_users_rent_details(user_id, rent_id):
    user_rent_details = db.get_collection('DvdRentalsCustomers').find(
        {"_id": user_id, "Rentals.rentalId": rent_id},
        {"Rentals.$": 1}
    )
    list_cur = list(user_rent_details)
    print(list_cur)
    amount = 0
    return_date = ""
    rent_date = ""
    movie_name = ""
    for data in list_cur:
        rent_date = data["Rentals"][0]["Rental Date"]
        movie_name = data["Rentals"][0]["Film Title"]
        try:
            amount = data["Rentals"][0]["Payments"][0]["Amount"]
            return_date = data["Rentals"][0]["Return Date"]
        except IndexError:
            None
    if not return_date:
        today_date = datetime.now(pytz.utc)
        duration = today_date - datetime.strptime(rent_date, '%Y-%m-%d %H:%M:%S.%f')
    else:
        duration = datetime.strptime(return_date, '%Y-%m-%d %H:%M:%S.%f') - datetime.strptime(rent_date, '%Y-%m-%d %H:%M:%S.%f')

    return jsonify({"Title": movie_name, "Duration": duration.days, "Rent_Date": rent_date, "Payment": amount})


@app.route('/films/a', methods=['GET'])
def films_to_rent_list():
    # today_date = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S.0"))
    today_date = "2005-09-01 04:30:24.0"
    # list_of_rented_films = db.get_collection('DvdRentalsCustomers').find(
    #     {'Rentals.Return Date': {"$gte": today_date}},
    #     {'Rentals.filmId': 1}
    # )
    list_of_rented_films = db.get_collection('DvdRentalsCustomers').distinct(
        'Rentals.filmId',
        {'Rentals.Return Date': {"$gte": today_date}}
    )
    list_of_avail = db.get_collection('DvdRentalsFilms').find(
        {'_id': {'$nin': list_of_rented_films}},
        {'Title': 1}
    )
    list_cur = list(list_of_avail)
    return jsonify(list_cur)


@app.route('/films/<int:film_id>', methods=['GET'])
def film_select_details(film_id):
    film_data = db.get_collection('DvdRentalsFilms').find(
        {'_id': film_id},
        {'Title': 1, 'Category': 1, 'Description': 1, 'Rating': 1, 'Rental Duration': 1}
    )
    list_cur = list(film_data)
    return jsonify(list_cur)


@app.route('/films/<int:film_id>/detailed', methods=['GET'])
def film_details(film_id):
    film_data = db.get_collection('DvdRentalsFilms').find(
        {'_id': film_id}
    )
    list_cur = list(film_data)
    return jsonify(list_cur)


@app.route('/films/<int:film_id>/rented', methods=['GET'])
def film_rented_by(film_id):
    film_data = db.get_collection('DvdRentalsCustomers').find(
        {'Rentals.filmId': film_id},
        {'First Name': 1, 'Last Name': 1}
    )
    list_cur = list(film_data)
    return jsonify(list_cur)


if __name__ == '__main__':
    app.run(debug=True)
