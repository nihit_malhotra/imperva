
function get_customers() {
    var url = "http://localhost:5000/userList"
    $("#response_div").html("")
    $("#response_div1").html("")
    $("#response_div2").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $.each(result, function(idx, data) {
                $("#response_div").append('<p><a href="javascript:get_user_details('+data['_id']+')">'+data['First Name']+ ' ' +data['Last Name']+'</a></p>');
            });
    }});
}

function get_user_details(id) {
    var url = "http://localhost:5000/user/"+id
    $("#response_div1").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $.each(result, function(idx, data) {
                $("#response_div1").append(
                '<p> Name: '+data['First Name']+' '+data['Last Name']+'</p><p>Address: '+data['Address']+'</br>City: '+data['City']+'</br>District: '+data['District']+'</br>Country: '+data['Country']+'</p><p><a href=javascript:user_film_list('+data['_id']+')>List Films</a></p>'
                );
            });
    }});
}

function user_film_list(id) {
    var url = "http://localhost:5000/user/"+id+"/rented"
     $("#response_div2").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $("#response_div2").html("<p>Filmss Rented</p>")
            $.each(result[0]['Rentals'], function(idx, data) {
                $("#response_div2").append(
                '</br><a href=javascript:get_rented_data('+id+','+data['rentalId']+')>'+data['Film Title']+'</a>'
                );
            });
    }});
}

function get_rented_data(user_id,film_id) {
    var url = "http://localhost:5000/user/"+user_id+"/rented/"+film_id
    $("#response_div").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $("#response_div").append(
                '<p>'+result['Title']+'</br>Rent Date: '+result['Rent_Date']+'</br> Duration:'+result['Duration']+'</br>Payment: $'+result['Payment']+'</p>'
                );
    }});
}

function get_films() {
    var url = "http://localhost:5000/films/a"
    $("#response_div").html("")
    $("#response_div1").html("")
    $("#response_div2").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $.each(result, function(idx, data) {
                $("#response_div").append('<a href="javascript:get_film_details('+data['_id']+')">'+data['Title']+'</a></br>');
            });
    }});
}

function get_film_details(id) {
    var url = "http://localhost:5000/films/"+id
    $("#response_div1").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $.each(result, function(idx, data) {
                $("#response_div1").append(
                '<p> Title: '+data['Title']+'</br>Category: '+data['Category']+'</br>Rating: '+data['Rating']+'</br>Rental Duration: '+data['Rental Duration']+'</br>Description: '+data['Description']+'</p><p><a href=javascript:who_rented('+data['_id']+')>Who Rented</a></br><a href=javascript:more_details('+data['_id']+')>More Details</a></p>'
                );
            });
    }});
}

function who_rented(id) {
    var url = "http://localhost:5000/films/"+id+"/rented"
    $("#response_div2").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $("#response_div2").html("<p>Films Rented By</p>")
            $.each(result, function(idx, data) {
                $("#response_div2").append(
                '</br>'+data['First Name']+' '+data['Last Name']+'</a>'
                );
            });
    }});
}

function more_details(id) {
    var url = "http://localhost:5000/films/"+id+"/detailed"
    $("#response_div2").html("")
    $.ajax({
        url: url,
        success: function(result){
            $("#request_div").html(url);
            $("#response_div2").html("<p>More Details</p>")
            $.each(result, function(idx, data) {
                $("#response_div2").append(
                'Length: '+data['Length']+'</br>Replacement Cost: '+data['Replacement Cost']+'</br>Special Features: '+data['Special Features']
                );
            });
            $("#response_div2").append("<p>Actors</p>");
            $.each(result[0]['Actors'], function(idx, data) {
                $("#response_div2").append(
                data['First name']+' '+data['Last name']+'</br>');
             });
    }});
}