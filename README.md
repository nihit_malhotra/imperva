**Requirments**

1. Clone/download this repository
2. Create a virtual env **[Help Link](https://pypi.org/project/pipenv/)**
3. Install the libraries in your pipenv
	- $ pipenv install -r requirements.txt
4. Install mongodb 
5. Import the two JSON into two collections use the following commands if using windows. The **mongoimport.exe** should be in installed directory of Mongodb
	- $ .\mongoimport.exe --host "localhost:27017" --collection **"DvdRentalsCustomers"** --db **"AppData"** --type "json" --file "<path>\imperva\DVDRentals-customers.json"
	- $ .\mongoimport.exe --host "localhost:27017" --collection **"DvdRentalsFilms"** --db **"AppData"** --type "json" --file "<path>\imperva\DVDRentals-films.json"
6. Start the API Server under server folder
	- python .\main.py
7. Open the home.html file under client to consume the API


---